from lf_util import *
import data_parser_LF3D as dp3
import random
import tensorflow as tf
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

from tensorflow.python import debug as tf_debug
tf.logging.set_verbosity(tf.logging.INFO)

###### Parameters #######

patch_size = 16
num_train_examples = int(1e5)
num_dev_examples = int(1e4)
d = 128
batch_size = 32
steps = 40000

###### Get Training Data and Split into Train\Dev ######
train_data, train_labels, dev_data, dev_labels = dp3.get_classification_data(num_train_examples, num_dev_examples, method="SIFT")

####################################################
def cnn_model_fn(features, labels, mode):
    # Input Layer for patches of different sizes
    input_img = features["x"]
    #tf.summary.image('input_img', input_img)
    print(input_img.shape)
    # First convolution, batch normalization and max pool
    conv1 = tf.layers.conv3d(inputs=input_img,filters=128,kernel_size=[3,3,3], padding="valid", activation=tf.nn.relu)
    print(conv1.shape) # shape here (128, 14, 14, 10, 128)
    #tf.summary.image('conv_1', tf.transpose(conv1[:1,:,:,:,:], [4,1,2,3,0]), max_outputs=25)
    pool1 = tf.layers.max_pooling3d(conv1, [2,2,2], strides=2)
    # shape here (128, 7, 7, 5)

    # Second convolution, batch normalization and max pool
    conv2 = tf.layers.conv3d(inputs=pool1,filters=128,kernel_size=[3,3,3], padding="valid", activation=None)
    batchnorm2 = tf.layers.batch_normalization(conv2)
    conv2 = tf.nn.relu(batchnorm2)
    # shape here (128, 5, 5, 3)
    pool2 = tf.layers.max_pooling3d(conv2, [5,5,3], strides=[5,5,3]) # Shape (batch_size, height, width, 128)
    # shape here (128, 1, 1, 1)
    print(pool2.get_shape().as_list())

    # Flatten the tensor (except for batch dimension)
    #flatten1 = tf.squeeze(pool2) # shape (batch_size, height, width, 128)
    # pool2_shape = pool2.shape.as_list()
    # flatten1 = tf.reshape(pool2, [-1,pool2_shape[1], pool2_shape[2], d])
    flatten1 = tf.reshape(pool2, ([-1, d]))

    # Two Final Dense Layers
    #dense1 = tf.layers.conv2d(inputs=pool2, filters=d, kernel_size=[1,1], padding='valid', activation=tf.nn.relu)
    dense1 = tf.layers.dense(inputs=flatten1, units=d, activation=tf.nn.relu)
    #out = tf.layers.conv2d(inputs=dense1, filters=2, kernel_size=[1,1], padding='valid', activation=tf.nn.relu)
    out = tf.layers.dense(inputs=dense1, units=2, activation=None)
    #out = tf.squeeze(out)

    # set up for predictions
    predictions = {
      "classes": tf.argmax(input=out, axis=1),
      "probabilities": tf.nn.softmax(out, name="softmax_tensor")
    }
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss
    loss = tf.losses.softmax_cross_entropy(labels, logits=out)

    # Define Optimizer
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(loss=loss,global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
	  "accuracy": tf.metrics.accuracy(
	      labels=tf.argmax(labels, axis=1), predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
	  mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def main(unused_argv):

    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=100)

    # Create TF Estimator
    patch_classifier = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir="/media/data/checkpoints/lffeatures/3D_classification")

    # Train Model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": train_data},
      y=train_labels,
      batch_size=batch_size,
      num_epochs=None,
      shuffle=True)

    patch_classifier.train(input_fn=train_input_fn,steps=steps, hooks=[logging_hook])

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": dev_data},
        y=dev_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = patch_classifier.evaluate(input_fn=eval_input_fn)
    print('######## Test Results ########')
    print(eval_results)
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data},
        y=train_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = patch_classifier.evaluate(input_fn=eval_input_fn)
    print('######## Train Results ########')
    print(eval_results)

if __name__ == "__main__":
    tf.app.run()


