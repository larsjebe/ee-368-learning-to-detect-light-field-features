import numpy as np
import glob
import os
from lf_util import *
import warnings
from random import shuffle

# returns the specified number of training and testing patches (3D volumes) and their labels. Half of them will be good and the other half will be bad patches. Method can either be "Harris" or "SIFT". 
def get_patches(num_train_patches, num_test_patches, method, dataset_path):    

    num_train_patches = int(num_train_patches/2)
    num_test_patches = int(num_test_patches/2)

    # get list of all csv files with good features
    csv_good_list = get_csv_good_list(method)

    # shuffle this list
    shuffle(csv_good_list)

    # extracts the specified number of good and bad examples from either the training or testing set as a numpy array.
    def extract(num_of_patches, subset):

        patches_good_full = np.empty((0,16,16, 12, 3))
        patches_bad_full = np.empty((0,16, 16, 12, 3))

        patch_count = 0
        image_count = 0

        for csv_good_name in csv_good_list:
            
            # get image and category identifier
            if method == "SIFT":
                image_identifier = csv_good_name[-22:-14]                
                category_identifier = csv_good_name.split("/")[-2][:-5]
            else:
                image_identifier = csv_good_name[-12:-4]
                category_identifier = csv_good_name.split("/")[-2][:-7]
            image_number = int(image_identifier[4:8])
                
            # get list of possible images and continue if it's a test image
            if subset == "train":
                possible_images = get_img_numbers(category_identifier, "train")
            elif subset == "test":
                possible_images = get_img_numbers(category_identifier, "test")
            if image_number not in possible_images:
                continue

            print("Reading from image: " + image_identifier + " in: " + category_identifier)           
            
            # get matching bad features (always from SIFT, not from Harris)
            query = "SIFT/" + category_identifier + "_SIFT/" + image_identifier + "*bad*.csv"
            csv_bad_name = glob.glob(query)[0]

            csv_good_data = np.genfromtxt(csv_good_name, delimiter = ",")[:4, :].T # first four rows only (no descriptors)
            csv_bad_data = np.genfromtxt(csv_bad_name, delimiter = ",")[:4, :].T # first four rows only (no descriptors)

            # counters for number of extracted patches and number of used images
            if patch_count >= num_of_patches:
                break;
            if patch_count + csv_good_data.shape[0] > num_of_patches:
                delta_count = num_of_patches-patch_count
                patch_count = num_of_patches
            else:
                delta_count = csv_good_data.shape[0]
                patch_count += csv_good_data.shape[0]

            image_count += 1

            # number of bad patches can differ from good patches
            delta_bad_patches = min(csv_bad_data.shape[0], delta_count)

            # get image matching the csv file
            img_path = dataset_path +  category_identifier + "/" + image_identifier + ".eslf.png"
            raw_LF = read_img(img_path)
            img = rawLF2rgb4D(raw_LF)

            # extract patches based on information in csv
            patches_good = np.zeros((delta_count, 16, 16, 12, 3))
            patches_bad = np.zeros((delta_bad_patches, 16, 16, 12, 3))
            m,n,o,p,_ = img.shape

            for i in range(delta_count):

                # good patches
                if method == "SIFT":
                    r = np.floor(csv_good_data[i,2]*6).astype(np.int32)
                else:
                    r = 6
                x = csv_good_data[i,0].astype(np.int32)
                y = csv_good_data[i,1].astype(np.int32)
                
                # coordinate translation from render to LF
                x_scale = round(x/3.7375).astype(np.int32)
                y_scale = round(y/3.7340).astype(np.int32)
                r_scale = round(r/3.7).astype(np.int32)
                
                x_start = max(x_scale-r_scale,0)
                x_end = min(x_scale+r_scale, n)
                y_start = max(y_scale-r_scale,0)
                y_end = min(y_scale+r_scale, m)
                
                # patch - rxrx1x12x3
                patch = img[y_start:y_end, x_start:x_end, 7, 1:13, :]

                for p in range(0, 11):
                    patch_loop = patch[:,:, p,:];
                    normpatch = cv2.resize(patch_loop, (16, 16), interpolation = cv2.INTER_LINEAR)
                    patches_good[i,:,:,p,:] = normpatch


                # bad patches
                if i < delta_bad_patches:
                    r = np.floor(csv_bad_data[i,2]*6).astype(np.int32)
                    x = csv_bad_data[i,0].astype(np.int32)
                    y = csv_bad_data[i,1].astype(np.int32)

                    # coordinate translation from render to LF
                    r_scale = round(r/3.7).astype(np.int32)
                    x_scale = round(x/3.7375).astype(np.int32)
                    y_scale = round(y/3.7340).astype(np.int32)
                

                    x_start = max(x_scale-r_scale,0)
                    x_end = min(x_scale+r_scale, n)
                    y_start = max(y_scale-r_scale,0)
                    y_end = min(y_scale+r_scale, m)
                    patch = img[y_start:y_end, x_start:x_end, 7, 1:13,:]

                    for p in range(0, 11):
                        patch_loop = patch[:,:, p,:];
                        normpatch = cv2.resize(patch_loop, (16, 16), interpolation = cv2.INTER_LINEAR)
                        patches_bad[i,:,:,p,:] = normpatch

            patches_good_full = np.concatenate([patches_good_full, patches_good], axis=0)
            patches_bad_full = np.concatenate([patches_bad_full, patches_bad], axis=0)

        # print stats for data retreival
        if patch_count == num_of_patches:
            print("Extracted %d patches from %d images" % (patch_count, image_count))
        else:
            print("Could only retrieve %d from the %d desired patches" % (patch_count, num_of_patches))

        return patches_good_full, patches_bad_full

    # extract good and bad patches
    patches_train_good, patches_train_bad = extract(num_train_patches, "train")
    patches_test_good, patches_test_bad = extract(num_test_patches, "test")

    return patches_train_good, patches_train_bad, patches_test_good, patches_test_bad

# this function returns list of csv-files that contain locations of good features. 
def get_csv_good_list(method):
    # set folder path
    if method == "SIFT":
        csv_folder_path = "SIFT/"
    else:
        csv_folder_path = "Harris/"

    # get list of feature files from every subdirectory
    subfolders = [x[0] for x in os.walk(csv_folder_path)][1:]

    if method == "SIFT":
        name_good_list = [glob.glob(folder + "/*good*.csv") for folder in subfolders]
    else:
        name_good_list = [glob.glob(folder + "/*.csv") for folder in subfolders]

    csv_good_list = [item for sublist in name_good_list for item in sublist]

    return csv_good_list


# returns good and bad patches with their labels in one-hot, shuffled
def get_classification_data(num_train_patches, num_test_patches, method="SIFT", dataset_path = "/media/data/LFSFM/dataset/"):

    if method == "SIFT":
        good, bad, good_t, bad_t = get_patches(num_train_patches, num_test_patches, method="SIFT", dataset_path=dataset_path)
    elif method == "HARRIS":
        good, bad, good_t, bad_t = get_patches(num_train_patches, num_test_patches, method="HARRIS", dataset_path=dataset_path)
    else:
        print("Method " + method + " not found")
        return -1

    train_data, labels = shuffle_and_label(good, bad)
    test_data, labels_t = shuffle_and_label(good_t, bad_t)

    return train_data, labels, test_data, labels_t

# this function takes two arrays, one containing good and the other one containing bad patches. Returns the training data as one single array, shuffled and labeled (on-hot). 
def shuffle_and_label(g, b):

    num_good = g.shape[0]
    num_bad = b.shape[0]

    print("Retrieved %d good and %d bad patches" % (num_good, num_bad))

    # labels: a GOOD patch:S is labeled with [0, 1] and a BAD patch with [1, 0]
    good_labels = np.ones((num_good))
    bad_labels = np.zeros((num_bad))
    labels = np.concatenate([good_labels, bad_labels])
    labels = np.stack([1-labels, labels], axis=1)
    data = np.concatenate([g, b], axis=0);
    seed = np.arange(labels.shape[0])

    # shuffle
    np.random.shuffle(seed)
    data = data[seed,:,:,:].astype(np.float32)
    labels = labels[seed,:].astype(np.float32)

    return data, labels

# high level function that returns shuffeled and labeled training patches and complete 3D light field slices from the test set. 
def get_detection_data(num_train_patches, num_test_images, method="SIFT", dataset_path = "/media/data/LFSFM/dataset/"):

    if method == "SIFT":
        good, bad, good_t, bad_t = get_patches(num_train_patches, 0, method="SIFT", dataset_path=dataset_path)
    elif method == "HARRIS":
        good, bad, good_t, bad_t = get_patches(num_train_patches, 0, method="HARRIS", dataset_path=dataset_path)
    else:
        print("Method " + method + " not found")
        return -1

    train_data, labels = shuffle_and_label(good, bad)
    test_images = get_test_images(num_test_images, dataset_path).astype(np.float32)

    return train_data, labels, test_images

# returns the specified number of images (3D light field slices, vertical center, all horizontal patches except for extreme views) from the test set
def get_test_images(num_test_images, dataset_path = "/media/data/LFSFM/dataset/"):

    if num_test_images == 0:
        return None
    
    # just used to get  a list of images
    csv_good_list = get_csv_good_list("SIFT")
    shuffle(csv_good_list)
    
    counter = 0

    for csv_good_name in csv_good_list:
        image_identifier = csv_good_name[-22:-14]
        category_identifier = csv_good_name.split("/")[-2][:-5]
        image_number = int(image_identifier[4:8])
        
        
        # get list of possible images
        possible_images = get_img_numbers(category_identifier, "test")
        if image_number not in possible_images:
            continue

        counter += 1
        # get image matching the csv file
        img_path = dataset_path +  category_identifier + "/" + image_identifier + ".eslf.png"
        raw_LF = read_img(img_path)
        img = rawLF2rgb4D(img_path)
        
        # select row of images
        img = img[:, :, 7, 1:13, :]

        if 'test_images' not in locals():
            test_images = np.empty((0, img.shape[0], img.shape[1], img.shape[2], 3))

        test_images = np.append(test_images,[img], axis=0)
        
        if counter == num_test_images:
            break
        
    return test_images.astype(np.float32)

# returns the image numbers for a given category as a list of lists, each sublist containing the image number that correspond to a single scene. (clustering images perfomred by Jayant, results saved in txt files in Split/)
def get_img_numbers(category, subset):
    file = "Split/" + category + "_split.txt"
    with open(file) as f:
        lines = f.readlines()
    lines = [x.strip() for x in lines]
    lines = [x.split(' ') for x in lines]
    if subset == "train":
        return [int(x) for x in lines[0]]
    elif subset == "test":
        return [int(x) for x in lines[1]]
    
    return -1
