clear all
% Directory of images
objectsDir = dir('G:\LFMVDataset\dataset\*.txt');

%%
% Go through each folder
for i = 1:length(objectsDir)
    % Open folder
    object = objectsDir(i).name;
    filePattern = fullfile(['G:\LFMVDataset\dataset\' objectName],...
        '*.render.png');
    files = dir(filePattern);
    
    % Make folder to store excel files
    [~, objectName, ~] = fileparts(object);
    mkdir(['G:\LFMVDataset\harris\' objectName '_harris\']);
    
    tic
    % Go through each image in a folder
    for k = 1:length(files)
        fullFileName = fullfile(myFolder, files(k).name);
        [FileDir, FileName, ~] = fileparts(fullFileName);
        [~, FileName, ~] = fileparts(FileName);
        img = (imread(fullFileName));
        imgGS = rgb2gray(img);

        % Detect Harris corners, find extrema
        corners = cornermetric(imgGS, 'Harris');
        harris_t = 0.0005;
        corners(corners < harris_t*max(corners(:))) = 0;
        imgExt = imregionalmax(corners);

        % Get top 500 keypoints
        [row, col] = find(imgExt == 1);
        responses = zeros(1,numel(row));
        for n = 1:numel(row)
            responses(n) = corners(row(n), col(n));
        end
        [responses, sortIdx] = sort(responses, 'descend');
        
        % If there are less than 500 keypoints, get them all
        if length(responses) < 500
            row_top = row(sortIdx(1:end)); 
            col_top = col(sortIdx(1:end));
        else 
            row_top = row(sortIdx(1:500)); 
            col_top = col(sortIdx(1:500));
        end 
        
        % Fixed scale 
        scale = 6;
        
        % Give scale and angle (1) to Harris features, use as input into
        % vl_sift to find corresponding SIFT features
        f_harris = [col_top'; row_top'; ones(1,length(row_top))*scale;...
            zeros(1,length(row_top))];
        [f_HS, d_HS] = vl_sift(single(imgGS), 'Frames', f_harris);

        % Write to csv file in object folder
        % Save features and descriptors
        csvwrite(['G:\LFMVDataset\harris\' objectName '_harris\' ...
            FileName '.csv'], [f_HS; double(d_HS)]); 
    end 
    toc
end