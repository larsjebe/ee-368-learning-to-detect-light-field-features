clear all;
% Load the image

% Set up SIFT command
run('C:\Users\Farah\Documents\Courses\EE368\Homework\ProblemSet6\hw6_data_and_code\vlfeat-0.9.21\toolbox\vl_setup');

% Path for all data
pathdir = ['D:\LFMVDataset\dataset'];

% Peak and edge threshold
peak_thresh = 10;
edge_thresh = 20;
edge_ind = 20; 
d_pixel = 10;

% Get the subfolders in the directory
folders = dir(pathdir);
filenames = {folders.name};
foldernames = filenames([folders.isdir]);

% Find features for all subfolders
for s = 4:length(foldernames)
    
    foldername_use = char(foldernames{s});
    disp(foldername_use);   
    
    % All path names  
    pathfile = [pathdir '\' foldername_use '\'];
    pathlookfile = [pathdir '\' foldername_use '\IMG_*.render.png'];
    pathsave = [pathdir '\SIFT\' foldername_use '_SIFT\'];

    % Make directory
    mkdir(pathsave);
    
    % Read all database images and get their SIFT features
    %Store SIFT descriptors for all database images
    filenames_all = dir(pathlookfile);
    filenames_all_names = {filenames_all.name};

    for i = 1:length(filenames_all_names)
        
        % Get name of image
        filename_use = char(filenames_all_names(i));
        im_path = [pathfile filename_use];
    
        [filepath1,name1,ext1] = fileparts(filename_use);
        [filepath2,name_keep,ext2] = fileparts(name1);
    
        % Read image
        im_data = imread(im_path);
        imgray_data = rgb2gray(im_data);
    
        % Get good features
        [f_good, d_good]= vl_sift(single(imgray_data), 'PeakThresh', peak_thresh, 'EdgeThresh', edge_thresh);
        
        % Save good features
        csv_file_good_name = [pathsave name_keep '_good_SIFT.csv'];
        SIFT_data_good_save = vertcat(f_good, double(d_good));
        csvwrite(csv_file_good_name, SIFT_data_good_save);
    
        % Get number of good features
        num_good_patches = size(f_good,2);
        f_good_len(i) = num_good_patches;
        
        % Get number of bad features
        num_bad_patches = num_good_patches;
                
        % Get a grid of all pixels
        x = 1:size(im_data,2);
        y = 1:size(im_data,1);
        [X Y] = meshgrid(x,y);
        rand_mat = zeros(size(im_data,1), size(im_data,2));
        
        % Pixels corresponding to good features set to 1
        good_ind = sub2ind(size(rand_mat), round(f_good(2,:)), round(f_good(1,:)));
        rand_mat(good_ind) = 1; 
          
        % Do dilation - avoid pixels within a d_pixel range
        SE = ones(2*d_pixel+1, 2*d_pixel+1);
        
        % Avoid all the egdge pixels
        rand_mat_dil = imdilate(rand_mat, SE);
        rand_mat_dil(1:edge_ind+1,:) = 1;
        rand_mat_dil(end-edge_ind:end,:) = 1;
        rand_mat_dil(:, 1:edge_ind+1) = 1;
        rand_mat_dil(:, end-edge_ind:end) = 1;
        
        % Get all pixels remaining - bad features
        bad_ind = find(rand_mat_dil == 0);
        bad_ind_shuff = bad_ind(randperm(length(bad_ind(:))));
        
        % Get a subset of them and store them as bad features
        f_bad(1,:) = X(bad_ind_shuff(1:num_bad_patches));
        f_bad(2,:) = Y(bad_ind_shuff(1:num_bad_patches));
        f_bad(3,:) = f_good(3,1:num_bad_patches);
        f_bad(4,:) = 0;
        csv_file_bad_name = [pathsave name_keep '_bad_SIFT.csv'];
        csvwrite(csv_file_bad_name, f_bad);
            
%         figure; imshow(im_data);
%         hold on; 
%         
%         vl_plotframe(f_good);
% 
%         figure; imshow(im_data);
%         hold on; 
%         vl_plotframe(f_bad);
%     
%         pause;
        
        % Clear variables
        clear f_bad; clear f_good; clear d_good;
        clear x; clear y; clear X; clear Y; clear rand_mat; clear good_ind; 
        clear rand_mat_dil; clear bad_ind; clear bad_ind_shuffle;
                 
        disp(i)

    end
    
    % Write number of features for each image in folder
    csv_file_misc_name = [pathsave name_keep '_peak_SIFT.csv'];
    misc_data = vertcat(f_good_len);
    csvwrite(csv_file_misc_name, misc_data);


end









