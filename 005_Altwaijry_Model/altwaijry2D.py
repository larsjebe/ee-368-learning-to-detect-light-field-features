from lf_util import *
import data_parser as dp
import random
import tensorflow as tf
import glob
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

from tensorflow.python import debug as tf_debug
# hooks = [tf_debug.LocalCLIDebugHook()]
tf.logging.set_verbosity(tf.logging.INFO)

###### Parameters #######

dataset_path = "/media/data/LFSFM/dataset/"
patch_size = 64
num_train_examples = int(1e5)
d = 128
batch_size = 128
steps = 2000



####################################################
def cond(shape_tens, *args):
    return shape_tens[1] >= 3

def body(shape_tens, x):
    with tf.variable_scope("loop", reuse=tf.AUTO_REUSE) as scope:
        #scope.reuse_variables()
        #kernel_shape = height, width, in_channels, out_channels]
        x_out = conv_relu(x, kernel_shape=[3,3,d,d], bias_shape=[d])
        x_out = tf.layers.batch_normalization(x_out)
    return tf.shape(x_out), x_out

def conv_relu(input_tensor, kernel_shape, bias_shape):
    weights = tf.get_variable("weights", kernel_shape,\
        initializer=tf.random_normal_initializer())

    biases = tf.get_variable("biases", bias_shape,\
        initializer=tf.constant_initializer(0.0))

    conv = tf.nn.conv2d(input_tensor, weights,\
        strides=[1, 1, 1, 1], padding='VALID')
    return tf.nn.relu(conv + biases)

def cnn_model_fn(features, labels, mode):
    # Input Layer for patches of different sizes
    input_img = features["x"]

    # First convolution, batch normalization and max pool
    conv1 = tf.layers.conv2d(inputs=input_img,filters=128,kernel_size=[3,3], strides=(2,2), padding="valid", activation=None)
    batchnorm1 = tf.layers.batch_normalization(conv1)
    conv1_act = tf.nn.relu(batchnorm1)
    pool1 = tf.layers.max_pooling2d(conv1_act, [3,3], strides=2)

    # Second convolution, batch normalization and max pool
    conv2 = tf.layers.conv2d(inputs=pool1,filters=128,kernel_size=[3,3], padding="valid", activation=None)
    batchnorm2 = tf.layers.batch_normalization(conv2)
    conv2_act = tf.nn.relu(batchnorm2)
    pool2 = tf.layers.max_pooling2d(conv2_act, [3,3], strides=2)

    # add zero padding so patch has odd size
    print("###########################")
    print(pool2.get_shape().as_list())
    p = 1 - pool2.get_shape().as_list()[1] % 2
    paddings = tf.constant([[0,0], [p,0], [p,0], [0,0]])
    tf.pad(pool2, paddings, "CONSTANT")
    # Convolution in Loop until tensor is in shape (None, 1, 1, d)
    _, out_tensor = tf.while_loop(cond, body, [tf.shape(conv2),conv2],\
            shape_invariants=[tf.TensorShape([4,]), tf.TensorShape([None, None, None, d])], back_prop=True)

    # Flatten the tensor (except for batch dimension)
    flatten1 = tf.reshape(out_tensor, [-1,d])

    # Two Final "Dense" Layers. Activation of last layer (softmax) included in loss function
    dense1 = tf.layers.conv2d(inputs=pool2, filters=d, kernel_size=[1,1], padding="valid", activation=tf.nn.relu)
    dense2 =  tf.layers.conv2d(inputs=dense1, filters=2, kernel_size=[1,1], padding="valid", activation=None)
    
    if mode == tf.estimator.ModeKeys.TRAIN:
        # remove singleton dimensions (for training)
        out = tf.reshape(dense2, ([-1,2]))
        # Calculate Loss
        loss = tf.losses.softmax_cross_entropy(labels, logits=out)

# this section is not done yet. predictions need to be branched off after every recursive layer (currently only the last one)
"""
    # set up for predictions
    if mode == tf.estimator.ModeKeys.PREDICT:
        # get heatmap by applying softmax
        heatmap = tf.nn.softmax(dense2)
        # upsample the heatmap to original image size
        heatmap_large = tf.image.resize_images(heatmap[:,:,:,1:], (image_sz[0], image_sz[1]))
        # perform non-maximum-suppression on a 3x3 window
        pooled = tf.nn.max_pool(heatmap_large, ksize=[1, 3, 3, 1], strides=[1,1,1,1], padding='SAME')
        found = tf.where(tf.equal(heatmap_large, pooled), heatmap_large,tf.zeros_like(heatmap_large))
        out = tf.cast(tf.greater(found,tf.convert_to_tensor(0.7)), tf.float32)
        predictions = {"heatmap": heatmap_large,
                "image_name": features["names"],
                "image" : input_img}
        tf.summary.image('heatmap', out, max_outputs=18)
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
"""

    # Define Optimizer
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(loss=loss,global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)


def main(unused_argv):

    num_dev_examples = 0
    train_data, train_labels, _, _ = dp.get_classification_data(num_train_examples, num_dev_examples, method="SIFT")
    
    # Create TF Estimator
    patch_classifier = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir="/media/data/checkpoints/lffeatures/altwaijry")

    # Train Model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": train_data},
      y=train_labels,
      batch_size=batch_size,
      num_epochs=None,
      shuffle=True)

    patch_classifier.train(input_fn=train_input_fn,steps=steps)

if __name__ == "__main__":
    tf.app.run()


