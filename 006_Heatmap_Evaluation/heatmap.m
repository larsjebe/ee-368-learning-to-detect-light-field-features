clear all

% Choose from 3 test image folders:
% 1) Render: 2D rendered images
% 2) 2Dslice: 2D light field slices
% 3) v2: 2D rendered images, second dataset (changed number of convolution
% layers)

imageType = 'render';
%imageType = '2Dslice';

switch imageType
    case 'render'
        m = 1404; n = 2022;
        factor = 12;
        PeakThresh = 10; EdgeThresh = 10;
        myFolder = 'H:\ee368\Heatmaps_Renders\';
        scale = 2.67;
        scaleRange = 1;
        %scale = [1 2 2.7 3 4 5 6 7 8 9 10];
        %scaleRange = 0.2:0.2:1;
    case '2Dslice'
        m = 376; n = 541;
        factor = 10;
        PeakThresh = 1; EdgeThresh = 10;
        myFolder = 'H:\ee368\Heatmaps_2DSlices\';
        scale = 1.33;
        %scale = 1:10;
        scaleRange = 1;
        %scaleRange = 0.2:0.2:1;
    case 'v2'
        m = 1404; n = 2022;
        factor = 7;
        PeakThresh = 1; EdgeThresh = 10;
        myFolder = 'H:\ee368\Heatmaps_v2\';
        scale = 2.67;
        %scale = 1:10;
        scaleRange = 1;
        %scaleRange = 0.2:0.2:1;   
end 

%%
% Get names of heatmap and image files
heatmapNames = dir(fullfile(myFolder, 'heatmap_*'));
imgNames = dir(fullfile(myFolder, 'IMG_*'));

% Threshold, use to adjust number of features returned in the heatmap. 
% Report uses a threshold of 0.6
thresh = 0.6; % 0.4 0.6 0.8

% Preallocate
precision = zeros(length(imgNames),length(scaleRange));
recall = zeros(length(imgNames),length(scaleRange));
precision_tot = zeros(length(scaleRange),1);
recall_tot = zeros(length(scaleRange), 1);
element = zeros(length(imgNames),3);
numSIFT = zeros(length(scaleRange), 1);

tic
for i = 1:length(scale)
    for k = 1:length(imgNames)
        % Read heatmap/2D slice files
        heatmapFileName = fullfile(myFolder, heatmapNames(k).name);
        imgFileName = fullfile(myFolder, imgNames(k).name);
        heatmap = (imread(heatmapFileName));
        img = imread(imgFileName);

        % Get regional max, non-binary
        heatmap_max = im2double(heatmap).*im2double(imregionalmax(heatmap));

        % Binarize with threshold
        heatmap_bin = imbinarize(heatmap_max, thresh);

        % Dilatae so it's easier to show (single pixels are too small)
        heatmap_mask = imdilate(heatmap_bin, ones(factor));
        
        % Get SIFT features of image
        imgGS = rgb2gray(img);
        [f, ~] = vl_sift(single(imgGS), 'PeakThresh', PeakThresh, ...
            'EdgeThresh', EdgeThresh);
        
        % If there are no features, then recall and precisino are 0
        if isempty(f)
            recall(k,i) = 0;
            precision(k,i) = 0;
        else
            % Get only features within a certain scale, round coordinates
            fThresh = f(:, f(3,:) < scale(i) + scaleRange);
            fThresh(:, fThresh(3,:) < scale(i) - scaleRange) = [];
            y = round(fThresh(1,:));
            x = round(fThresh(2,:));

            % Create binary map of SIFT coordinates, apply and with
            % heatmap to get # of matches
            f_bin = zeros(m, n); 
            for j = 1:length(y)
                f_bin(x(j), y(j)) = 1;
            end 
            f_dil = imdilate(f_bin, ones(factor));
            matches = and(f_dil, heatmap_bin);
            
            % Get number of matches, total number of SIFT features, and
            % total number of heatmap features for each image
            element(k,1) = sum(matches(:));       % true positives
            element(k,2) = length(y);             % relevant elements
            element(k,3) = sum(heatmap_bin(:));   % true + false positives
            
            % Calculate recall and precision for each image
            recall(k,i) = element(k,1)/element(k,2);
            precision(k,i) = element(k,1)/element(k,3);
        end 
    end 
    
    % Get total number of SIFT features, recall, and precision for each
    % scale
    numSIFT(i) = sum(element(:,2));
    recall_tot(i) = sum(element(:,1))/sum(element(:,2));
    precision_tot(i) = sum(element(:,1))/sum(element(:,3));
end 
toc

%% Plot and save figure for recall/precision vs. scale for rendered images
h = figure;
plot(scale, 100*precision_tot, 'o--', scale, 100*recall_tot, 'o--')
legend('Precision', 'Recall')
xlabel('Scale of SIFT features (\pm 1)'); ylabel('Value (%)')
set(h, 'Color', 'w'); grid on
saveas(h, 'render.pdf')
% Can export to PDF 
%export_fig render.pdf
