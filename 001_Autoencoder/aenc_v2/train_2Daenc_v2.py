from lf_util import *
import random
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, ZeroPadding2D, Cropping2D
from keras.models import Model
from keras import backend as K
from keras import optimizers
import glob
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '3'


###### Parameters #######

folder_path = "/media/data/LFSFM/dataset/trees/"
max_images = 20
patch_size = 64
train_size = int(4e3)

#########################

for num, img_path in enumerate(glob.glob(os.path.join(folder_path, '*.eslf.png'))):
    if num >= max_images:
        break
    img = read_img(img_path)
    print('Reading image %d / %d' % (num+1, max_images), end='\r')
    image = rawLF2gray4D(img)
    image_data_4D = []
    image_data_4D.append(image)

(k,l,_,_) = image.shape

num_images = len(image_data_4D)
# build training data by randomly selecting patch_sizexpatch_size patches from the image data
train_data = np.ndarray(shape=(train_size, patch_size, patch_size), dtype = np.float32)
for i in range(train_size):
    pos = [random.randint(0,num_images-1), random.randint(0,k-patch_size), \
    random.randint(0,l-patch_size), random.randint(0,13), random.randint(0,13)]
    train_data[i,:,:] = image_data_4D[pos[0]][pos[1]:pos[1]+patch_size, pos[2]:pos[2]+patch_size, pos[3], pos[4]]
    if not (i % 1000):
        print('Selecting random patches: %d / %d' % (i, train_size), end="\r")

train_data = np.reshape(train_data, (train_size, patch_size, patch_size, 1))

########### DONE ##########
# select patches of image instead of complete slice (patch_sizexpatch_size pixels)
# change hyperparameters to compression rate of 4
# design 3D autoencoder with same compression rate and compare to 2D


input_img = Input(shape = (patch_size, patch_size, 1))

x = Conv2D(64, (5, 5), activation='relu', padding='same')(input_img)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(128, (5, 5), activation='relu', padding='same')(x)
x = Conv2D(4, (3, 3), activation='relu', padding='same')(x)
encoded = MaxPooling2D((2, 2), padding='same')(x)

print(x.shape.as_list()) # compression rate: 4. from 128x128x1 to 32x32x4

x = Conv2D(4, (3, 3), activation='relu', padding='same')(encoded)
x = Conv2D(128, (5, 5), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
x = Conv2D(64, (5, 5), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
decoded = Conv2D(1, (5, 5), activation='sigmoid', padding='same')(x)


autoencoder = Model(input_img, decoded)
print(autoencoder.summary())
# specify optimizer properties
opt = optimizers.Adadelta(lr=0.5, rho=0.95, epsilon=None, decay=0.0)

# compile model
autoencoder.compile(optimizer=opt, loss='binary_crossentropy')

# train model
autoencoder.fit(train_data, train_data, epochs=10, batch_size=128)

# save model
autoencoder.save_weights('weights2D_v2-1.h5')

############ Build and save model used for testing

k = 376
l = 541

pad_k = (4 - (k % 4)) % 4
pad_l = (4 - (l % 4)) % 4

input_img = Input(shape = (k, l, 1))

x = ZeroPadding2D(padding=((pad_k, 0), (pad_l,0)), data_format=None)(input_img)
x = Conv2D(64, (5, 5), activation='relu', padding='same')(x)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(128, (5, 5), activation='relu', padding='same')(x)
x = Conv2D(4, (3, 3), activation='relu', padding='same')(x)
encoded = MaxPooling2D((2, 2), padding='same')(x)

x = Conv2D(4, (3, 3), activation='relu', padding='same')(encoded)
x = Conv2D(128, (5, 5), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
x = Conv2D(64, (5, 5), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
x = Cropping2D(cropping=((pad_k, 0), (pad_l,0)), data_format=None)(x)
decoded = Conv2D(1, (5, 5), activation='sigmoid', padding='same')(x)


test_autoencoder = Model(input_img, decoded)
print(test_autoencoder.summary())

# specify optimizer properties
opt = optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)

# compile model
test_autoencoder.compile(optimizer=opt, loss='binary_crossentropy')

# save model
test_autoencoder.save('test_aenc2D_v2.h5')











