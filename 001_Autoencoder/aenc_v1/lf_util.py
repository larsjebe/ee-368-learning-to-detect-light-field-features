import cv2
import numpy as np

def read_img(image_path):
    img = cv2.imread(image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = img.astype(np.float32)
    img /= 255.
    return img

def write_img(img, image_path):
    img *= 255
    img = img.astype(np.uint8)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return cv2.imwrite(image_path, img)

def rawLF2gray4D(raw_LF):
    img = cv2.cvtColor(raw_LF, cv2.COLOR_RGB2GRAY)
    (m,n) = img.shape
    k = int(m/14)
    l = int(n/14)
    image = np.ndarray(shape=(k, l, 14, 14), dtype = np.float32)
    for x in range(14):
        for y in range(14):
            image[:,:,x,y] = img[x::14, y::14]
    return image




