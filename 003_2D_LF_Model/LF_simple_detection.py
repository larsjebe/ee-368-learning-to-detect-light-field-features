from lf_util import *
import data_parser_LF2D as dp
import random
import tensorflow as tf
import os
import scipy
import sys
os.environ['CUDA_VISIBLE_DEVICES'] = '3'


from tensorflow.python import debug as tf_debug
# hooks = [tf_debug.LocalCLIDebugHook()]
tf.logging.set_verbosity(tf.logging.INFO)

###### Parameters #######

patch_size = 16
num_examples = int(3e3)
d = 128
batch_size = 128
steps = int(num_examples/batch_size)*3
num_dev_images = 1
image_sz = (376, 541)

####################################################
def cnn_model_fn(features, labels, mode):
    print(mode)
    print("############################")
    # Input Layer for patches of different sizes
    input_img = features["x"]
    # tf.summary.image('input_img', input_img[:18,:,:,:], max_outputs=18)

    # First convolution, batch normalization and max pool
    conv1 = tf.layers.conv2d(inputs=input_img,filters=128,kernel_size=[3,3],  padding="valid", activation=tf.nn.relu)
    print(conv1.shape) # shape here (128, 15, 15, 128)
    # tf.summary.image('conv_1', tf.transpose(conv1[:18,:,:,:1], [0,1,2,3]), max_outputs=18)
    pool1 = tf.layers.max_pooling2d(conv1, [2,2], strides=2)

    # Second convolution, batch normalization and max pool
    conv2 = tf.layers.conv2d(inputs=pool1,filters=128,kernel_size=[3,3], padding="valid", activation=None)
    conv2 = tf.nn.relu(conv2)
    batchnorm2 = tf.layers.batch_normalization(conv2)
    pool2 = tf.layers.max_pooling2d(batchnorm2, [5,5], strides=5) # Shape (batch_size, height, width, 128)

    print(pool2.get_shape().as_list())

    # Flatten the tensor (except for batch dimension)
    # flatten1 = tf.squeeze(pool2) # shape (batch_size, height, width, 128)
    # pool2_shape = pool2.shape.as_list()
    # flatten1 = tf.reshape(pool2, [-1,pool2_shape[1], pool2_shape[2], d])

    # Two Final "Dense" Layers
    dense1 = tf.layers.conv2d(inputs=pool2, filters=d, kernel_size=[1,1], padding="valid", activation=tf.nn.relu)
    dense2 =  tf.layers.conv2d(inputs=dense1, filters=2, kernel_size=[1,1], padding="valid", activation=None)
    # flatten1 = tf.reshape(pool2, ([-1, d]))
    # dense1 = tf.layers.dense(inputs=flatten1, units=d, activation=tf.nn.relu)
    #dense2 = tf.layers.dense(inputs=dense1, units=2, activation=None)

    print(dense2.get_shape().as_list())

    if mode == tf.estimator.ModeKeys.TRAIN:
        # remove singleton dimensions (for training)
        out = tf.reshape(dense2, ([-1,2]))
        # Calculate Loss
        loss = tf.losses.softmax_cross_entropy(labels, logits=out)


    # set up for predictions
    if mode == tf.estimator.ModeKeys.PREDICT:
        # get heatmap by applying softmax
        heatmap = tf.nn.softmax(dense2)
        # upsample the heatmap to original image size
        heatmap_large = tf.image.resize_images(heatmap[:,:,:,1:], (image_sz[0], image_sz[1]))
        # perform non-maximum-suppression on a n x n window
        pooled = tf.nn.max_pool(heatmap_large, ksize=[1, 15, 15, 1], strides=[1,1,1,1], padding='SAME')
        found = tf.where(tf.equal(heatmap_large, pooled), heatmap_large,tf.zeros_like(heatmap_large))
        out = tf.cast(tf.greater(found,tf.convert_to_tensor(0.7)), tf.float32)
        predictions = {"heatmap": heatmap_large,
                "image_name": features["names"],
                "image" : input_img}
        tf.summary.image('heatmap', out, max_outputs=18)
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Define Optimizer
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(loss=loss,global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)


def main(argv):

    if len(argv) > 1:
        command = argv[1]
    else:
        command = ""
    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    # tensors_to_log = {"probabilities": "softmax_tensor"}
    # logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=1000)


    # Create TF Estimator
    patch_detector = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir="/media/data/checkpoints/lffeatures/LF2D_detection_task")

    ###### Get Training Data and Split into Train\Dev ######
    if command != "train" and command != "predict":
        train_data, train_labels, dev_images, image_names = dp.get_detection_data(num_examples, num_dev_images, method="SIFT")
    elif command == "train":
        train_data, train_labels, dev_images, image_names = dp.get_detection_data(num_examples, 0, method="SIFT")
    elif command == "predict":
        train_data = None
        train_labels = None
        dev_images, image_names = dp.get_test_images(num_dev_images)


    # Train Model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": train_data},
      y=train_labels,
      batch_size=batch_size,
      num_epochs=None,
      shuffle=True)


    if command != "predict":
        patch_detector.train(input_fn=train_input_fn,steps=steps, hooks=None)

    # Predict on complete pictures
    predict_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x" : dev_images,
          "names" : image_names},
      y=None,
      batch_size=16,
      num_epochs=1,
      shuffle=False)

    if command != "train":
        predictions = patch_detector.predict(input_fn=predict_input_fn)

        for idx, pred_dict in enumerate(predictions):
            heatmap = pred_dict['heatmap']
            image = pred_dict['image']
            image_name = pred_dict['image_name']
            print(heatmap.shape)
            path_heatmap = "heatmaps_LF2D/heatmap_" + str(image_name) + ".png"
            path_image = "heatmaps_LF2D/IMG_" + str(image_name) + ".png"
            scipy.misc.imsave(path_heatmap , heatmap[:,:,0])
            scipy.misc.imsave(path_image, image)


if __name__ == "__main__":
    tf.app.run()


