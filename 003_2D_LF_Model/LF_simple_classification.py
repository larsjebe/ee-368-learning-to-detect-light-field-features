from lf_util import *
import data_parser_LF2D as dp
import random
import tensorflow as tf
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '2'

from tensorflow.python import debug as tf_debug
# hooks = [tf_debug.LocalCLIDebugHook()]
tf.logging.set_verbosity(tf.logging.INFO)

###### Parameters #######

patch_size = 16
num_train_examples = int(1e3)
num_dev_examples = int(1e3)
d = 128
batch_size = 32
steps = 40000

###### Get Training Data and Split into Train\Dev ######
train_data, train_labels, dev_data, dev_labels = dp.get_classification_data(num_train_examples, num_dev_examples, method="SIFT")

####################################################
def cnn_model_fn(features, labels, mode):
    # Input Layer for patches of different sizes
    input_img = features["x"]

    # First convolution, batch normalization and max pool
    conv1 = tf.layers.conv2d(inputs=input_img,filters=128,kernel_size=[3,3], padding="valid", activation=tf.nn.relu)
    print(conv1.shape) # shape here (128, 15, 15, 128)
    pool1 = tf.layers.max_pooling2d(conv1, [3,3], strides=2)

    # Second convolution, batch normalization and max pool
    conv2 = tf.layers.conv2d(inputs=pool1,filters=128,kernel_size=[3,3], padding="valid", activation=None)
    batchnorm2 = tf.layers.batch_normalization(conv2)
    conv2 = tf.nn.relu(batchnorm2)
    pool2 = tf.layers.max_pooling2d(conv2, [4,4], strides=4) # Shape (batch_size, height, width, 128)

    print(pool2.get_shape().as_list())

    # Flatten the tensor (except for batch dimension)
    #flatten1 = tf.squeeze(pool2) # shape (batch_size, height, width, 128)
    # pool2_shape = pool2.shape.as_list()
    # flatten1 = tf.reshape(pool2, [-1,pool2_shape[1], pool2_shape[2], d])
    flatten1 = tf.reshape(pool2, ([-1, d]))

    # Two Final Dense Layers
    #dense1 = tf.layers.conv2d(inputs=pool2, filters=d, kernel_size=[1,1], padding='valid', activation=tf.nn.relu)
    dense1 = tf.layers.dense(inputs=flatten1, units=d, activation=tf.nn.relu)
    #out = tf.layers.conv2d(inputs=dense1, filters=2, kernel_size=[1,1], padding='valid', activation=tf.nn.relu)
    out = tf.layers.dense(inputs=dense1, units=2, activation=None)
    #out = tf.squeeze(out)

    # set up for predictions
    predictions = {
      "classes": tf.argmax(input=out, axis=1),
      "probabilities": tf.nn.softmax(out, name="softmax_tensor")
    }
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    # Calculate Loss
    loss = tf.losses.softmax_cross_entropy(labels, logits=out)

    # Define Optimizer
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(loss=loss,global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
	  "accuracy": tf.metrics.accuracy(
	      labels=tf.argmax(labels, axis=1), predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
	  mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def main(unused_argv):

    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=1000)

    # Create TF Estimator
    patch_classifier = tf.estimator.Estimator(model_fn=cnn_model_fn, model_dir="/media/data/checkpoints/lffeatures/LF2D_classification_task")

    # Train Model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
      x={"x": train_data},
      y=train_labels,
      batch_size=batch_size,
      num_epochs=None,
      shuffle=True)

    # saver = tf.train.Saver()
    patch_classifier.train(input_fn=train_input_fn,steps=steps, hooks=[logging_hook])

    # saver.save(sess, "Models/Model_v0-0", global_step=steps)


    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": dev_data},
        y=dev_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = patch_classifier.evaluate(input_fn=eval_input_fn)
    print('######## Test Results ########')
    print(eval_results)
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data},
        y=train_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = patch_classifier.evaluate(input_fn=eval_input_fn)
    print('######## Train Results ########')
    print(eval_results)

if __name__ == "__main__":
    tf.app.run()


